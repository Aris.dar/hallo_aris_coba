import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'hello world',
      home: Material(
        child: Center(
          child: Text("Hello World"),
        ),
      ),
    );
  }
}
